const Sequelize = require("sequelize");
const { connection, Op } = require("../config/sequelize.js");

const Snap = connection.define(
  "snaps",
  {
    id: {
      type: Sequelize.BIGINT,
      primaryKey: true
    },
    name: Sequelize.STRING,
    link: Sequelize.STRING,
    user: Sequelize.STRING,
    status: Sequelize.STRING,
    date: Sequelize.DATE,
    likes: Sequelize.INTEGER,
    data: Sequelize.TEXT
  },
  {
    timestamps: false
  }
);

connection.sync();

module.exports = { Snap };

const os = require("os");
const server = require("server");
const { get, error } = server.router;
const { render, type, send, json, status } = server.reply;
const fs = require("fs");

const { Snap } = require("./models/Snap");
const webshot = require('webshot');

const IPADDRESS = os.networkInterfaces().ens3[0].address;

atob = (b64Encoded) => { 
  return Buffer.from(b64Encoded, 'base64').toString() 
};

// overload console
var { format } = require('util');
var log_file = fs.createWriteStream(__dirname + '/logs/debug.log', {flags : 'w'});
var log_stdout = process.stdout;

console.debug = function(d) { //
  log_file.write(`${new Date()}:: `+format(d) + '\n');
  log_stdout.write(`\x1b[1m\x1b[33m ${new Date()}:: \x1b[0m `+format(d) + '\x1b[0m \n');
};

// Get url screenshot send it
getImage = async (url) => {
  console.debug(`Getting ${url} image`);
  var bufs = [];
  var options = {
    screenSize: {
      width: 700, height: 512
    }, 
    userAgent: 'Mozilla/5.0 (iPad; CPU OS 9_3_2 like Mac OS X) AppleWebKit/601.1.46 (KHTML, like Gecko) Version/9.0 Mobile/13F69 Safari/601.1'
  };
  var readStream = webshot(url, options);
  readStream.on('data', function(data) {
    bufs.push(data)
  });
  
  var end = new Promise((resolve, reject) => {
    readStream.on('end', () => {
      resolve(Buffer.concat(bufs));
    });
    readStream.on('error', (error) => {
      console.debug(`Nope on ${url} ${error}`);
      reject(error);    
    });
  })
  
  return end;
}

// This code is blocking, it would be better if it did not.
const getUser = get("/user/:user", async ctx => {
  console.debug(`getUser :: ${ctx.params.user}`);
  var data = [];  
  snaps = await Snap.findAll({ where: { user: ctx.params.user }});
  for (i = 0; i < snaps.length; i++) {
    item = snaps[i].dataValues;
    if (item.status == 'active') {
      buffer = await getImage(item.link); 
      base64encoded = (new Buffer(buffer, 'utf8').toString('base64'));
    } else {
      base64encoded = item.data;
    }
    data.push({ 'address': item.link, 'encoded': item.name, 'data': base64encoded });
  }
  if (data) {
    console.debug('getUser return');
    return json(data);
  } else {
    console.debug('Hell no ya fucked yp');
  }
});
  
const Save = get("/save/:url", async ctx => {
  console.debug(`save :: ${ctx.params.url} ${ctx.ip}`);
  
  var data = [];
  let url = atob(ctx.params.url);
  
  buffer = await getImage(url); 
  var base64encoded = (new Buffer(buffer, 'utf8').toString('base64'));
  if (base64encoded){
    Snap.destroy({ where: { name: ctx.params.url }});
    Snap.create({
      name: ctx.params.url,
      link: url,
      user: 'nthcolumn',
      status: 'active',
      date: new Date(),
      likes: 0,
      data: base64encoded
    });
  }
  console.debug('save return');
  data.push({ 'address': url, 'encoded': ctx.params.url, 'data': base64encoded });
  return json(data);
});

const Remove = get("/remove/:id", ctx => {
  console.debug(`remove :: ${ctx.params.id} `);
  Snap.destroy({ where: { name: ctx.params.id }});
  return status(200);
});

// Home
const Main = get("/", async (ctx) => {
  console.debug(`\x1b[33mMain route requested by ${ctx.ip}`);
  return render("index.html");
});

// start monitor webserver
console.log(`\x1b[2J\x1b[2;2H\x1b[1m\x1b[46m Server on http://${IPADDRESS} port 80. \x1b[0m`);
process.argv.forEach(function (val, index, array) {
  console.debug('args '+index + ': ' + val);
});

server({ port: 80 }, [ Main, getUser, Save, Remove, error(ctx => send(ctx.error.message)) ]);
console.debug(`\x1b[32mServer Up \x1b[0m`);
